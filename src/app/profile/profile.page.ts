import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../_services/authentication/authentication.service';
import {User} from '../_models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: User;

  constructor(
      private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.user = this.authService.currentUserValue;
  }

}
