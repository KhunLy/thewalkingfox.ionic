import { Component, OnInit } from '@angular/core';
import {ItineraryService} from '../_services/itinerary/itinerary.service';
import {Itinerary} from '../_models/itinerary';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-itineraries',
  templateUrl: './itineraries.page.html',
  styleUrls: ['./itineraries.page.scss'],
})
export class ItinerariesPage implements OnInit {

  private itineraries: Array<Itinerary> = [];

  constructor(
      private itineraryService: ItineraryService,
      private router: Router,
      private loadingController: LoadingController
  ) { }

  async ngOnInit() {
    const loader = await this.loadingController.create({
      message: 'Loading ...'
    });
    loader.present();
    this.itineraryService.getAll().subscribe(data => {
      this.itineraries = data; console.log(data);
      loader.dismiss();
    }, error => {
      loader.dismiss();
    });
  }

  doRefresh(event) {
    this.itineraryService.getAll().subscribe(data => {
      this.itineraries = data;
      event.target.complete();
    }, error => {
      event.target.complete();
    });
  }

  select(itinerary: Itinerary) {
    this.itineraryService.selectitineray(itinerary).then(x => {
      x.subscribe(y => {this.router.navigate(['/steps']); } );
    });
  }

}
