import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Itinerary} from '../../_models/itinerary';
import {AuthenticationService} from '../authentication/authentication.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItineraryService {

  private currentItinerarySubject: BehaviorSubject<Itinerary>;
  public currentItinerary: Observable<Itinerary>;

  constructor(
      private storage: Storage,
      private httpClient: HttpClient,
      private auth: AuthenticationService
  ) {
    this.storage.get('currentItinerary').then(data => {
      this.currentItinerarySubject = new BehaviorSubject<Itinerary>(data);
      this.currentItinerary = this.currentItinerarySubject.asObservable();
    });
  }

  public get currentitinerayValue(): Itinerary {
    return this.currentItinerarySubject != null ? this.currentItinerarySubject.value : null;
  }

  public getAll(): Observable<Array<Itinerary>> {
    // this.httpClient.get<Array<Itinerary>>(environment.apiEndPoint + '' + this.auth.currentUserValue.id).subscribe(data => {
    return this.httpClient.get<Array<Itinerary>>(environment.apiEndPoint + 'ItinerariesMobile/GetItinerariesOfCustomer/' + this.auth.currentUserValue.id);
  }

  public async selectitineray(itinerary: Itinerary): Promise<Observable<Itinerary>> {
       await this.storage.set('currentItinerary', itinerary);
       return new Observable(observer => {
          if (itinerary != null) {

            this.currentItinerarySubject = new BehaviorSubject<Itinerary>(itinerary);
            observer.next(itinerary);
          } else {
            observer.next(null);
          }
        });
  }
}
