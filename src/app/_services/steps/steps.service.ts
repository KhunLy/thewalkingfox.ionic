import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Storage } from '@ionic/storage';
import {Step} from '../../_models/step';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../../_models/user';
import {Itinerary} from '../../_models/itinerary';

@Injectable({
  providedIn: 'root'
})
export class StepsService {
  private currentStepSubject: BehaviorSubject<Step>;
  public currentStep: Observable<Step>;

  public callbacks: Array<any> = [];

  constructor(
      private httpClient: HttpClient,
      private storage: Storage
  ) {
    this.currentStepSubject = new BehaviorSubject<Step>(null);
    this.currentStep = this.currentStepSubject.asObservable();
  }

  public async getCurrentStep() {
    const data = await this.storage.get('currentStep');
    this.currentStepSubject = new BehaviorSubject<Step>(data);
    this.currentStep = this.currentStepSubject.asObservable();
    return this.currentStep;
  }

  public get currentStepValue(): Step {
    return this.currentStepSubject != null ? this.currentStepSubject.value : null;
  }

  async getAllSteps(): Promise<Observable<Array<Step>>> {
    const it = await this.storage.get('currentItinerary');
    return this.httpClient.get<Array<Step>>(environment.apiEndPoint + 'ItinerariesStepsMobile/GetStepsByItinerary/' + it.itineraryID);
  }

  public async selectStep(step: Step): Promise<Observable<Step>> {
    await this.storage.set('currentStep', step);
    this.currentStep =
    new Observable(observer => {
      if (step != null) {

        this.currentStepSubject = new BehaviorSubject<Step>(step);
        observer.next(step);
      } else {
        observer.next(null);
      }
    });
    this.invoke();
    return this.currentStep;
  }

  subscribe(cb) {
    this.callbacks.push(cb);
  }

  invoke() {
    for (const cb of this.callbacks) {
      console.log(42)
      cb();
    }
  }
}
