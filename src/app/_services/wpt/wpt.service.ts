import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Wpt} from '../../_models/wpt';
import {StepsService} from '../steps/steps.service';
import {Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class WptService {

  constructor(
      private httpClient: HttpClient,
      private stepsService: StepsService,
      private storage: Storage
  ) { }

  public async getWpts(): Promise<Observable<Array<Wpt>>> {
    const id = (await this.storage.get('currentStep')).stepID;
    return this.httpClient.get<Array<Wpt>>(environment.apiEndPoint + 'WaypointsMobile/GetWaypointsOfStep/' + id);
  }
}
