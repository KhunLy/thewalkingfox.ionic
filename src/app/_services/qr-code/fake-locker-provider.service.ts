import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Locker} from '../../_models/locker';

@Injectable({
  providedIn: 'root'
})
export class FakeLockerProviderService {

  constructor(

  ) { }

  public getLockers(id: number): Observable<Array<Locker>> {
    return new Observable(observer => {
      setTimeout(() => {
        const list = [];
        for (let i = 1; i < 11; i++) {
          const rand = Math.random();
          const locker = <Locker>{
            id: i,
            name: (rand * 10000000).toFixed().toString(),
            isOpen: false,
            place: `A{i}`
          };
          list.push(locker);
        }
        observer.next(list);
      }, 1000);
    });
  }
}
