import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Order} from '../../_models/order';
import {OrderDetails} from '../../_models/order.details';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
      private httpClient: HttpClient,
      private authService: AuthenticationService) { }

  public getAll(): Observable<Array<Order>> {
    return this.httpClient.get<Array<Order>>(environment.apiEndPoint + 'OrdersMobile/GetOrdersOfCustomersMobile/' + this.authService.currentUserValue.id);
  }

  public getDetails(orderID: number): Observable<OrderDetails> {
    return this.httpClient.get<OrderDetails>(environment.apiEndPoint + 'OrdersMobile/GetOrdersInfosMobile/' + orderID);
  }
}
