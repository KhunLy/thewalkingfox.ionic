import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../_models/user';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private storage: Storage) {
    this.storage.get('currentUser').then(data => {
      this.currentUserSubject = new BehaviorSubject<User>(data);
      this.currentUser = this.currentUserSubject.asObservable();
    });
  }

  public get currentUserValue(): User {
    return this.currentUserSubject != null ? this.currentUserSubject.value : null;
  }

  login(username: string, password: string): Observable<User> {
      return new Observable(observer => {
        setTimeout(() => {
          if (username.toLowerCase() === 'jane.doe@gmail.com' && password === 'Test1234=') {
            const user = <User>{
              username: 'jane.doe@gmail.com',
              id: 1022,
              firstName: 'Jane',
              lastName: 'Doe',
              password: null,
              token: 'supertoken'
            };
            this.storage.set('currentUser', user);
            this.currentUserSubject = new BehaviorSubject<User>(user);
            observer.next(user);
          } else {
            observer.next(null);
          }
        }, 2000);
      });
  }

  logout() {
    // remove user from local storage to log user out
    this.storage.remove('currentUser');
    this.currentUserSubject.next(null);
    this.storage.remove('currentItinerary');
  }
}
