import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './_guards/auth.guard';
import {ItineraryGuard} from './_guards/itinerary.guard';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard] },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule', canActivate: [AuthGuard] },
  // { path: 'about', loadChildren: './about/about.module#AboutPageModule', canActivate: [AuthGuard, ItineraryGuard] },
  // { path: 'camera', loadChildren: './tabs/camera/camera.module#CameraPageModule' },
  { path: 'itineraries', loadChildren: './itineraries/itineraries.module#ItinerariesPageModule', canActivate: [AuthGuard] },
  { path: 'steps', loadChildren: './steps/steps.module#StepsPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
