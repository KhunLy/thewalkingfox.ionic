import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Coords} from '../../_models/coords';
import { Map, latLng, tileLayer, Layer, marker, icon, point, polyline } from 'leaflet';
import 'node_modules/leaflet-gpx';
import {Storage} from '@ionic/storage';
import {Step} from '../../_models/step';
import {WptService} from '../../_services/wpt/wpt.service';
import {StepsService} from '../../_services/steps/steps.service';
import {Observable} from 'rxjs';

declare var L;
@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit, AfterContentInit {
  error: string;
  coords: Coords;
  compt = 0;
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  // // Marker for the top of Mt. Ranier
  // start = marker([ 50.476, 4.471 ], {
  //   icon: icon({
  //     iconSize: [ 25, 41 ],
  //     iconAnchor: [ 13, 41 ],
  //     iconUrl: 'leaflet/marker-icon.png',
  //     shadowUrl: 'leaflet/marker-shadow.png'
  //   })
  // });
  //
  // // Marker for the parking lot at the base of Mt. Ranier trails
  // arrival = marker([ 50.44688, 4.448 ], {
  //   icon: icon({
  //     iconSize: [ 25, 41 ],
  //     iconAnchor: [ 13, 41 ],
  //     iconUrl: 'leaflet/marker-icon.png',
  //     shadowUrl: 'leaflet/marker-shadow.png'
  //   })
  // });

  currentPos = marker([ 50.44688, 4.448 ], {
    icon: icon({
      iconSize: [ 40, 40 ],
      iconAnchor: [ 40, 40 ],
      iconUrl: 'assets/pin-icon-wpt.png'
      // shadowUrl: 'assets/pin-shadow.png'
    })
  });

  // Path from paradise to summit - most points omitted from this example for brevity
  // route = polyline(
  //     [[ 50.476, 4.471 ], [ 50.44688, 4.448 ]]);

  // Layers control object with our two base layers and the three overlay layers
  layersControl = {
    baseLayers: {
      'Street Maps': this.streetMaps,
      'Wikimedia Maps': this.wMaps
    },
    overlays: {
      // 'Start': this.start,
      // 'Arrival': this.arrival,
      // 'Route': this.route
    }
  };


  // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  options = {
    layers: [ this.streetMaps, /*this.start, this.arrival,*/ this.currentPos ],
    zoom: 12,
    // center: latLng([ 46.879966, -121.726909 ])
  };

  private step: Step;

  private map: Map;

  private lastGPX;

  private steps;

  constructor(
      private geoloc: Geolocation,
      private storage: Storage,
      private wptService: WptService,
      private stepsService: StepsService
  ) { }

  async ngOnInit() {
    // this.steps = await this.stepsService.getCurrentStep().then(x => {
    //   console.log(1);
    //   this.refreshMap();
    // });
    // console.log(2);
    // this.step = await this.stepsService.getCurrentStep();
    // console.log(3);
    //
    // console.log(this.step);
    // if (this.step != null) {
    //   console.log(0);
    //   this.step.subscribe(data => this.ngAfterContentInit());
    // }
    this.ngAfterContentInit();
    this.stepsService.subscribe(() => this.ngAfterContentInit());
  }



  async ngAfterContentInit() {
    // console.log(1);
    this.step = await this.storage.get('currentStep');
      this.map.eachLayer( l => {
        console.log(l);
        if (Object.keys(l).filter(k => k === '_gpx').length > 0) {
          this.map.removeLayer(l);
        }
      });
    // console.log(45);
    setTimeout(() => { this.map.invalidateSize(true); }, 500);
    if (this.coords == null) {
      this.map.setView({lat: 50.850198, lng: 4.448264}, 14);
      this.geoloc.getCurrentPosition().then(position => {
        this.coords = position.coords;
        this.currentPos.setLatLng([this.coords.latitude, this.coords.longitude]);
        console.log(46);
        this.compt++;
        this.map.setView({lat: this.coords.latitude, lng: this.coords.longitude}, 14);
      }).catch(error => this.error = error.message);
    } else {
      console.log(47);
      this.map.setView({lat: this.coords.latitude, lng: this.coords.longitude}, 14);
    }
    this.updatePosition();
    console.log(this.step);
    if (this.step == null) { return; }
    const gpxFile = 'assets/gpx' + this.step.stepGPX + '.gpx';
    // const gpxFile = 'assets/gpx/' + 'TMB' + this.i + '.gpx';
    // @ts-ignore


    (await this.wptService.getWpts()).subscribe(data => {
      this.lastGPX = new L.GPX(gpxFile, {
        async: true,
        marker_options: {
          startIconUrl: 'assets/pin-icon-start.png',
          endIconUrl: 'assets/pin-icon-end.png',
          // shadowUrl: 'assets/pin-shadow.png',
          wptIconUrls: 'assets/pin-icon-wpt.png'
        },
        polyline_options: {
          color: 'darkCyan',
          opacity: 0.75,
          weight: 3,
          lineCap: 'round'
        }
      }).on('loaded', (e) => {
        this.map.fitBounds(e.target.getBounds());
        console.log(this.map);
      }).addTo(this.map);
      for (const wpt of data) {
        console.log(wpt);
        this.map.addLayer(marker([ parseFloat(wpt.latitude), parseFloat(wpt.longitude)], {
            icon: icon({
              iconSize: [ 25, 41 ],
              iconAnchor: [ 13, 41 ],
              iconUrl: 'assets/pin-icon-wpt.png',
              shadowUrl: 'assets/pin-shadow.png'
            })
          }));
      }
    });
    // map.fitBounds(this.route.getBounds(), {
    //   padding: point(10, 10),
    //   maxZoom: 14,
    //   animate: true
    // });
  }

  async onMapReady(map: Map) {
    this.map = map;
  }

  updatePosition() {
    this.geoloc.watchPosition({
      enableHighAccuracy: true,
      timeout: 500,
      maximumAge: 0
    }).subscribe(position => {
      this.coords = position.coords;
      if (this.coords !== undefined) {
        this.currentPos.setLatLng([this.coords.latitude, this.coords.longitude]);
      }
      this.compt++;
    }, error =>  this.error = error.message);
  }


}
