import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MapPage } from './map.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

const routes: Routes = [
  {
    path: '',
    component: MapPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    LeafletModule.forRoot()
  ],
  declarations: [MapPage],
  providers: [Geolocation]
})
export class MapPageModule {}
