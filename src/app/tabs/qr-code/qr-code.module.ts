import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QrCodePage } from './qr-code.page';
import {QRCodeModule} from 'angularx-qrcode';
import {FakeLockerProviderService} from '../../_services/qr-code/fake-locker-provider.service';
import {QrCodeModalComponent} from '../order-details/qr-code-modal/qr-code-modal.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

const routes: Routes = [
  {
    path: '',
    component: QrCodePage
  }
];

@NgModule({
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AngularFontAwesomeModule
  ],
  declarations: [QrCodePage],
  providers: [
    FakeLockerProviderService
  ]
})
export class QrCodePageModule {}
