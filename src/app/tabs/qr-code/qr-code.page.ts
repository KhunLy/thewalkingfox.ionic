import { Component, OnInit } from '@angular/core';
import {FakeLockerProviderService} from '../../_services/qr-code/fake-locker-provider.service';
import {Locker} from '../../_models/locker';
import {ActionSheetController, LoadingController, ModalController} from '@ionic/angular';
import {OrderService} from '../../_services/order/order.service';
import {Order} from '../../_models/order';
import {Router} from '@angular/router';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.page.html',
  styleUrls: ['./qr-code.page.scss'],
})
export class QrCodePage implements OnInit {

  private data: Array<Order>;

  constructor(
      private actionSheetCtrl: ActionSheetController,
      private loadingController: LoadingController,
      private orderService: OrderService,
      private router: Router
  ) {
  }

  async ngOnInit() {
      const loader = await this.loadingController.create({
          message: 'Loading ...'
      });
      loader.present();
      this.orderService.getAll().subscribe(
          data => {
              this.data = data;
              loader.dismiss();
          }
      );
  }
  async onItemClicked(item: Order) {
      this.router.navigateByUrl('/tabs/order-details/' + item.orderID);
  }

  doRefresh(event) {
      this.orderService.getAll().subscribe(
          data => {
              this.data = data;
              event.target.complete();
          }
      );
  }

}
