import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: '../tabs/map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: 'qr-code',
        children: [
          {
            path: '',
            loadChildren: '../tabs/qr-code/qr-code.module#QrCodePageModule'
          }
        ]
      },
      {
        path: 'order-details/:orderID',
        children: [
          {
            path: '',
            loadChildren: '../tabs/order-details/order-details.module#OrderDetailsPageModule'
          }
        ]
      },
      // {
      //   path: 'camera',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: '../tabs/camera/camera.module#CameraPageModule'
      //     }
      //   ]
      // },
      {
        path: '',
        redirectTo: '/tabs/map',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/map',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
