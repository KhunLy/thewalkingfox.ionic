import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrderDetailsPage } from './order-details.page';
import {QrCodeModalComponent} from './qr-code-modal/qr-code-modal.component';
import {QRCodeModule} from 'angularx-qrcode';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsPage
  }
];

@NgModule({
  entryComponents: [
    QrCodeModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    QRCodeModule,

  ],
  declarations: [OrderDetailsPage, QrCodeModalComponent]
})
export class OrderDetailsPageModule {}
