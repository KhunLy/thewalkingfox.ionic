import {Component, Input, OnInit} from '@angular/core';
import {Locker} from '../../../_models/locker';
import {ModalController} from '@ionic/angular';
import {OrderDetails} from '../../../_models/order.details';

@Component({
  selector: 'app-qr-code-modal',
  templateUrl: './qr-code-modal.component.html',
  styleUrls: ['./qr-code-modal.component.scss'],
})
export class QrCodeModalComponent implements OnInit {

  @Input() order: OrderDetails;

  constructor(
      private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  close() {
    this.modalCtrl.dismiss();
  }

  saveAndClose() {
    // this.locker.isOpen = true;
    this.modalCtrl.dismiss();
  }

}
