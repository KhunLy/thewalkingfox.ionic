import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../_services/order/order.service';
import {OrderDetails} from '../../_models/order.details';
import {ActivatedRoute} from '@angular/router';
import {ActionSheetController, LoadingController, ModalController} from '@ionic/angular';
import {QrCodeModalComponent} from './qr-code-modal/qr-code-modal.component';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {

  data: OrderDetails;
  constructor(
      private orderService: OrderService,
      private activedRoute: ActivatedRoute,
      private actionSheetCtrl: ActionSheetController,
      private modalCtrl: ModalController,
      private loadingController: LoadingController
  ) { }

  ngOnInit() {

  }

  async ionViewDidEnter() {
      const loader = await this.loadingController.create({
          message: 'Loading ...'
      });
      this.activedRoute.params.subscribe(params => {
          this.orderService.getDetails(params.orderID).subscribe(data => {
              this.data = data;
              loader.dismiss();
          });
      });
  }

  async openActionsSheet() {
    const actionSheet = await this.actionSheetCtrl.create({
        buttons: [{
          // text: item.isOpen ? 'Lock' : 'Unlock',
          text: 'Unlock',
          // icon: item.isOpen ? 'lock' : 'unlock',
          icon: 'unlock',
            handler: () => {
                // item.isOpen = !item.isOpen;
            }
        }, {
            text: 'QR-Code',
            icon: 'qr-scanner',
            handler: () => this.openQrCodeModal()
        }, {
            text: 'Cancel',
            icon: 'Close'
        }]
    });
    actionSheet.present();
  }

  openQrCodeModal() {
    this.modalCtrl.create({
      component: QrCodeModalComponent,
      componentProps: { order: this.data },
      cssClass: 'small-modal'
    }).then((modal) => modal.present());
  }

}
