import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../_services/authentication/authentication.service';
import {LoadingController, ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {StepsService} from '../_services/steps/steps.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string;
  password: string;
  constructor(
      private authService: AuthenticationService,
      private toastCtrl: ToastController,
      private router: Router,
      private loadingController: LoadingController,
      private stepService: StepsService
  ) { }

  ngOnInit() {
  }

  canSubmit(): boolean {
    return this.password == null || this.username == null;
  }

  async login() {
    const loader = await this.loadingController.create({
      message: 'Loading ...'
    });
    loader.present();
    this.authService.login(this.username, this.password).subscribe( async user => {
      if (user == null) {
        const toast = await this.toastCtrl.create({
          message: 'Bad Credentials.',
          duration: 2000
        });
        toast.present();
      } else {
        this.stepService.invoke();
        this.router.navigate(['/']);
      }
      loader.dismiss();
    });
  }
}
