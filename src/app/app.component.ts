import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AuthenticationService} from './_services/authentication/authentication.service';
import {Router} from '@angular/router';
import {ItineraryService} from './_services/itinerary/itinerary.service';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Accueil',
      url: '/tabs',
      icon: 'home'
    }, {
          title: 'Profil',
          url: '/profile',
          icon: 'person'
      }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthenticationService,
    private router: Router,
    private itinerayService: ItineraryService,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout() {
      this.storage.set('currentStep', null);
      this.storage.clear().then(x => {
          this.authService.logout();
          this.router.navigate(['/login']);
      });
  }
  changeitineray() {
      this.router.navigate(['/itineraries']);
  }
}
