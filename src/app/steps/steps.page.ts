import { Component, OnInit } from '@angular/core';
import {StepsService} from '../_services/steps/steps.service';
import {Step} from '../_models/step';
import {Storage} from '@ionic/storage';
import {Itinerary} from '../_models/itinerary';
import {Router} from '@angular/router';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.page.html',
  styleUrls: ['./steps.page.scss'],
})
export class StepsPage implements OnInit {

  private steps: Array<Step> = [];
  private itinerary: Itinerary;
  constructor(private stepsService: StepsService, private storage: Storage, private router: Router) { }

  async ngOnInit() {
    this.itinerary = await this.storage.get('currentItinerary');
    this.stepsService.getAllSteps().then(data => data.subscribe(sts => {
      this.steps = sts;
    }));
  }

  select(step: Step) {
    this.stepsService.selectStep(step).then(x => {
      x.subscribe(y => {
        this.router.navigate(['/tabs/map']);
      });
    });
  }

}
