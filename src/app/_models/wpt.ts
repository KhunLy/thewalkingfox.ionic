export interface Wpt {
    waypointID: number;
    waypointLabel: string;
    waypointDescription: string;
    latitude: string;
    longitude: string;
    waypointPoints: number;
    waypointIsDeleted: boolean;
    waypointCreationDate: string;
    waypointCreatedBy: number;
    waypointModificationDate: string;
    waypointModifiedBy: number;
}
