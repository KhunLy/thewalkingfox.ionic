export interface OrderDetails {
    orderID: number;
    orderReference: string;
    lockerDepart: string;
    rueDepart: string;
    numeroDepart: string;
    boiteDepart: string;
    zipCodeDepart: string;
    villeDepart: string;
    paysDepart: string;
    numLockerDepart: number;
    lockerRetour: string;
    rueRetour: string;
    numeroRetour: string;
    boiteRetour: string;
    zipCodeRetour: string;
    villeRetour: string;
    paysRetour: string;
    numLockerRetour: number;
    satus: string;
    orderQRCode: string;
}
