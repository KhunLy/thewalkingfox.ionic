export interface Step {
    stepID: number;
    stepReference: string;
    stepOrder: number;
    stepName: string;
    stepDescription: string;
    stepDistance: number;
    stepDuration: number;
    stepElevationPos: number;
    stepElevationNeg: number;
    stepGPX: string;
}
