export interface Order {
    orderID: number;
    orderReference: string;
    lockerDepart: string;
    paysDepart: string;
    lockerRetour: string;
    paysRetour: string;
}
