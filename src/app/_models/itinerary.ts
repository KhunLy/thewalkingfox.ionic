export interface Itinerary {
    itineraryID: number;
    itineraryReference: string;
    itineraryName: string;
    itineraryDescription: string;
}
