export interface Locker {
    id: number;
    name: string;
    code: string;
    userId: number;
    place: string;
    isOpen: boolean;
}
